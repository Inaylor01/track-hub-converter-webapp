from django.shortcuts import render
from django.http import HttpResponse
import json
from .create_resources import Create_Resources

# Create your views here.

cr = Create_Resources()
def get_resources(request):
    if request.method == 'GET':
        track_link = request.GET.get('hubUrl', '')
        file_name = str(request.GET.get('fileName'))
        res = ""
        if  "/contents.txt" in file_name:
            res = cr.create_contents_txt(track_link)
        elif "genome.txt" in file_name:
            _, genome, file = file_name.split("/")
            res = cr.create_genome_txt(track_link, genome, "")
        elif "annots.xml" in file_name:
            _, genome, file = file_name.split("/")
            res = cr.create_annots_xml(track_link, genome)
        #print(res)
        return HttpResponse(res, content_type="text/xml")

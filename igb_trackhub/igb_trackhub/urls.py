from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('trackhub/', include('track_link_generator.urls')), #name of URL for link generator is currently track hub. Can be changed
    path(r'rest_api/', include('track_rest_api.urls'))
]
